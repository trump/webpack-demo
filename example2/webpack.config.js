var path = require('path');

var CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin');

var entry = {
    'company/index-1210-en_US': './scs/src/desktop/company/index.js'
    , 'settings/billing': './scs/src/desktop/settings/billing.js'
    , 'settings/calls': './scs/src/desktop/settings/calls.js'

    , 'overview': './scs/src/desktop/overview.js'
    , 'activities': './scs/src/desktop/activities.js'
    , 'contacts/index': './scs/src/desktop/contacts/index.js'
    , 'settings/index': './scs/src/desktop/settings/index.js'

};

module.exports = {
    entry: entry
    , plugins: [
        new CommonsChunkPlugin("desktop.share.js", Object.keys(entry))
    ]
    , output: {
        filename: '[name].js'//, //this is the default name, so you can skip it
        , path: path.join(__dirname, "./scs/src/desktop-build")
    }
    , module: {
        loaders: [
            {
                test: /\.js$/
                , loader: 'rc-loader'
            }
        ]
    }
};